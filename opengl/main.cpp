#include "gui.h"
#include "shader.h"
#include "window.h"

#include <fstream>
#include <chrono>
#include <random>
#include <ranges>
#include <cstring>

#define GLASSERT do {GLuint const glerror = glGetError(); if(glerror != GL_NONE) std::cerr << __LINE__ << ": " << glerror << std::endl;} while(false)

struct vec3 {
    float x, y, z;
};


int main() {
    Window window;
    GUI gui(window.pointer);

    ImGuiIO &io = ImGui::GetIO();
    io.FontGlobalScale = 1.f / std::min(window.xscale, window.yscale);
    io.Fonts->AddFontFromFileTTF(FONT_DIR "Roboto-Medium.ttf", 16.f / io.FontGlobalScale, nullptr,
                                 io.Fonts->GetGlyphRangesCyrillic());

    auto const readFile = [](char const *const filename) noexcept {
        std::ifstream in(filename);
        char buffer[16384] = {'\0'};
        in.read(buffer, sizeof(buffer));
        return std::string(buffer);
    };

    Shader const screen =
    {
        readFile(SHADER_DIR "fullscreen_quad.vert").c_str(),
        readFile(SHADER_DIR "fragment.glsl").c_str(),
    };
    Shader const compute =
    {
        readFile(SHADER_DIR "jacobi.glsl").c_str(),
    };

    GLuint vao;
    glGenVertexArrays(1, &vao);

    GLuint buffer[5];
    glGenBuffers(5, buffer);

    [[maybe_unused]] auto makeDiag = [](std::size_t dim, float value) {
        auto data = std::views::iota(0ul, dim * dim) | std::views::transform([value, dim](std::size_t i) {
                        if (i % dim == i / dim) {
                            return value;
                        } else {
                            return 0.f;
                        }
                    }) |
                    std::views::common;
        return std::vector<float>(std::ranges::begin(data), std::ranges::end(data));
    };

    auto makeDiagonalDominant = [](std::size_t dim, float value) {
        auto data = std::views::iota(0ul, dim * dim) | std::views::transform([value, dim](std::size_t i) {
                        if (i % dim == i / dim) {
                            return value;
                        } else {
                            return value / float(2 * dim);
                        }
                    }) |
                    std::views::common;
        return std::vector<float>(std::ranges::begin(data), std::ranges::end(data));
    };

    auto makeRHS = [](std::size_t dim, float value) {
        auto data = std::views::iota(0ul, dim) | std::views::transform([value, dim](std::size_t) {
                        return value;
                    }) |
                    std::views::common;
        return std::vector<float>(std::ranges::begin(data), std::ranges::end(data));
    };

    uint dim = 10000;
    auto mat = makeDiagonalDominant(dim, 1000.f);
    auto rhs = makeRHS(dim, 5.f);
    auto initial = makeRHS(dim, 0.f);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[0]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(sizeof(float) * dim * dim), mat.data(), GL_DYNAMIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffer[0]);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[1]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(sizeof(float) * dim), rhs.data(), GL_DYNAMIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffer[1]);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[2]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(sizeof(float) * dim), initial.data(), GL_DYNAMIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffer[2]);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[3]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(sizeof(float) * dim), nullptr, GL_DYNAMIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, buffer[3]);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[4]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(sizeof(float) * dim), nullptr, GL_DYNAMIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, buffer[4]);
    uint I = 0;
    std::vector<float> solution(dim, 0.f);
    std::vector<float> residualAbs(dim, 10.f);
    auto maxResidualAbs = *std::ranges::max_element(residualAbs);
    while (!window.shouldClose()) {
        int width, height;
        glfwGetFramebufferSize(window.pointer, &width, &height);

        glBindVertexArray(vao); {
            glDisable(GL_DEPTH_TEST);

            glUseProgram(screen);
            glDrawArrays(GL_TRIANGLES, 0, 6);
        } {
            glUseProgram(compute);
            glUniform1ui(glGetUniformLocation(compute, "dim"), uint(dim));
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
            glDispatchCompute((dim + 31) / 32, 1, 1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        } {
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[4]);
            void *const data = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
            std::memcpy(residualAbs.data(), data, sizeof(float) * dim);
            maxResidualAbs = *std::ranges::max_element(residualAbs);
            glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2 + (I % 2), buffer[3]);
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2 + (I + 1) % 2, buffer[2]);
            if (maxResidualAbs < 1e-3f) {
                glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[3]);
                void *const solutionData = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
                std::memcpy(solution.data(), solutionData, sizeof(float) * dim);
                glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
                break;
            }
            ++I;
        }

        glBindVertexArray(0);
        gui.render();
    }

    glDeleteVertexArrays(1, &vao);

    for (auto &elm: solution) {
        std::cout << elm << std::endl;
    }
    std::cout << "Итераций: " << I + 1 << std::endl;
}
