
#version 430 core

layout(local_size_x = 32, local_size_y = 32, local_size_z = 1) in;

layout(std430, binding = 0) readonly buffer MatrixA {
    int data[];
}matrixa;

layout(std430, binding = 1) readonly buffer MatrixB {
    int data[];
} matrixb;

layout(std430, binding = 2) writeonly buffer MatrixC {
    int data[];
} matrixc;



void main() {
    const uint row = gl_GlobalInvocationID.x;
    const uint col = gl_GlobalInvocationID.y;

    if(row >= 1024 || col >= 1024) {
        return;
    }

    int sum = 0;
    for(uint i = 0; i < 1024; i++) {
        sum += matrixa.data[row * 1024 + i] * matrixb.data[i * 1024 + col];
    }

    matrixc.data[row * 1024 + col] = sum;
}