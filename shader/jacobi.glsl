#version 430 core

layout(local_size_x = 32, local_size_y = 1, local_size_z = 1) in;

layout(std430, binding = 0) readonly buffer MatrixA {
    float data[];
}matrixa;

layout(std430, binding = 1) readonly buffer RHS {
    float data[];
} rhs;

layout(std430, binding = 2) readonly buffer Prev {
    float data[];
} prev;

layout(std430, binding = 3) writeonly buffer Next {
    float data[];
} next;

layout(std430, binding = 4) writeonly buffer ResidualAbs{
    float data[];
} residualAbs;

uniform uint dim;

void jacobiIteration(){
    const uint row = gl_GlobalInvocationID.x;

    if (row >= dim) {
        return;
    }
    float diag = 0.f;
    float sum = 0.f;
    float residualRow = 0.f;
    residualAbs.data[row] = 0.f;
    for (uint i = 0; i < dim; i++) {
        if(i != row){
            sum += matrixa.data[row * dim + i] * prev.data[i];
        }else{
            diag = matrixa.data[row * dim + i];
            residualRow = diag * prev.data[row];
        }
    }
    next.data[row] = (rhs.data[row] - sum) / diag;
    residualRow += sum - rhs.data[row];
    residualRow = abs(residualRow);
    residualAbs.data[row] = residualRow;
}

void main() {
    jacobiIteration();
}