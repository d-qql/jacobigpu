#include <vector>
#include <array>
#include <cmath>
#include <ranges>
#include <iostream>
#include <algorithm>

std::vector<float> jacobi(std::vector<float> const &A, std::vector<float> const &b, std::vector<float> const &init) {
    std::array<std::vector<float>, 2> prevnext = {init, std::vector<float>(init.size())};
    int it = 0;
    std::vector<float> residualAbs(b.size(), 1.f);
    float residualRowAbs = 0.f;
    float resMaxAbs = 1;
    while (resMaxAbs >= 1e-3) {
        if (it % 2 == 0) {
            auto &[prev, next] = prevnext;
            for (uint i = 0; i < b.size(); ++i) {
                float diag = 0.f;
                for (uint k = 0; k < b.size(); k++) {
                    if (k != i) {
                        next[i] -= A[i * b.size() + k] * prev[k];
                        residualRowAbs += A[i * b.size() + k] * prev[k];
                    } else {
                        diag = A[i * b.size() + k];
                        residualRowAbs += diag * prev[k];
                    }
                }
                next[i] += b[i];
                next[i] /= diag;
                residualRowAbs -= b[i];
                residualAbs[i] = std::abs(residualRowAbs);
            }
        } else {
            auto &[next, prev] = prevnext;
            for (uint i = 0; i < b.size(); ++i) {
                float diag = 0.f;
                for (uint k = 0; k < b.size(); k++) {
                    if (k != i) {
                        next[i] -= A[i * b.size() + k] * prev[k];
                        residualRowAbs += A[i * b.size() + k] * prev[k];
                    } else {
                        diag = A[i * b.size() + k];
                        residualRowAbs += diag * prev[k];
                    }
                }
                next[i] += b[i];
                next[i] /= diag;
                residualRowAbs -= b[i];
                residualAbs[i] = std::abs(residualRowAbs);
            }
        }
        resMaxAbs = *std::ranges::max_element(residualAbs);
        ++it;
        std::cout << it << std::endl;
    }
    return prevnext[1];
}

int main() {
    auto makeDiagonalDominant = [](std::size_t dim, float value) {
        auto data = std::views::iota(0ul, dim * dim) | std::views::transform([value, dim](std::size_t i) {
                        if (i % dim == i / dim) {
                            return value;
                        } else {
                            return value / float(2 * dim);
                        }
                    }) |
                    std::views::common;
        return std::vector<float>(std::ranges::begin(data), std::ranges::end(data));
    };

    auto makeRHS = [](std::size_t dim, float value) {
        auto data = std::views::iota(0ul, dim) | std::views::transform([value, dim](std::size_t) {
                        return value;
                    }) |
                    std::views::common;
        return std::vector<float>(std::ranges::begin(data), std::ranges::end(data));
    };

    uint dim = 10000;
    auto mat = makeDiagonalDominant(dim, 1000.f);
    auto rhs = makeRHS(dim, 5.f);
    auto initial = makeRHS(dim, 0.f);

    auto solution = jacobi(mat, rhs, initial);
    for(auto const& elm:solution) {
        std::cout << elm << std::endl;
    }
}